﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    private string layerName = "HipHopLayer";
    private int layerId;
    private Animator animator;

    // Parameters for transitioning between animations
    public float transitionTime = 1f;
    private float timeAcum = 0f;  
    private int start = 1;
    private int end = 0;

    void Start()
    {
        animator = GetComponent<Animator>();
        layerId = animator.GetLayerIndex(layerName);
    }

    // Transition animations
    void Update()
    {
        if (timeAcum < transitionTime)
        {
            timeAcum += Time.deltaTime;
            animator.SetLayerWeight(layerId, Mathf.Lerp(start, end, timeAcum));
        }
    }

    public void Idle()
    {
        timeAcum = 0f;
        start = 1;
        end = 0;
    }

    public void Dance()
    {
        timeAcum = 0f;
        start = 0;
        end = 1;
    }
}
