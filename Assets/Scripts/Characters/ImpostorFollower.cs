﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpostorFollower : MonoBehaviour
{
    public Transform originalHead;
    public Transform originalLeftHand;
    public Transform originalRightHand;
    public Transform originalPlacement;

    public Transform impostorHead;
    public Transform impostorLeftHand;
    public Transform impostorRightHand;
    public Transform impostorPlacement;

    public Vector3 offset;
    
    // Start is called before the first frame update
    void Start()
    {
        impostorHead.parent.position = impostorPlacement.position - originalPlacement.position + offset;
    }

    // Models are fixed to the position of their placement, their stand
    // But height of the VR headset is used
    void FixedUpdate()
    {
        Vector3 headHeight = new Vector3(0f, originalHead.position.y, 0f);

        impostorHead.localPosition = headHeight;
        impostorLeftHand.localPosition = originalLeftHand.localPosition - originalHead.localPosition + headHeight;
        impostorRightHand.localPosition = originalRightHand.localPosition - originalHead.localPosition + headHeight;

        impostorHead.parent.LookAt(originalHead);
    }
}
