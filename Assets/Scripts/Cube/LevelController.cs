﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    public AudioSource mainAudioSource; // Controlled by Hearing on mix table
    private AudioSource themeAudioSource; // Controlled by FX
    private AudioClip clip;

    private float delay = 3.55f;

    public GameObject[] swords;
    public GameObject[] uiPointers;

    private bool isPaused = false;
    private float gameTime = 0f;
    private float clipLength = 0f;

    private ScoreHandler scoreHandler;
    private CubeSpawner spawner;
    private MainUIHandler uiHandler;
    private AnimationHandler[] animateds;

    // Start on select song mode
    void Start()
    {
        scoreHandler = FindObjectOfType<ScoreHandler>();
        spawner = FindObjectOfType<CubeSpawner>();
        uiHandler = FindObjectOfType<MainUIHandler>();
        themeAudioSource = GetComponent<AudioSource>();
        animateds = FindObjectsOfType<AnimationHandler>();

        EnableSwords(false);
        uiHandler.ChooseSongUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPaused && !mainAudioSource.isPlaying && !spawner.IsWorking() && clipLength > 0f && gameTime > clipLength)
        {
            EndGame(true);
        }

        if (!isPaused)
        {
            gameTime += Time.deltaTime;
        }

        // Pause
        if (OVRInput.GetDown(OVRInput.Button.Start))
        {
            if (mainAudioSource.isPlaying || spawner.IsWorking())
            {
                PauseGame();
            }
            else if (isPaused)
            {
                ResumeGame();
            }
        }
    }

    public void SetupGame(string song, AudioClip aClip)
    {
        mainAudioSource.clip = aClip;
        clipLength = aClip.length;
        Debug.Log("Clip Length is: " + aClip.length + " seconds.");
        scoreHandler.SetupScoreboard(song);
        spawner.SetupSpawner(aClip);
        uiHandler.EnableStartButton();
    }

    public void StartGame()
    {
        Invoke("StartSongWithDelay", delay);
        isPaused = false;
        Time.timeScale = 1;
        spawner.Init();
        gameTime = 0f;
        scoreHandler.Start();
        uiHandler.HideAll();
        EnableSwords(true);
        EnableUiPointers(false);
        themeAudioSource.Stop();
    }

    private void PauseGame()
    {
        Time.timeScale = 0;
        mainAudioSource.Pause();
        spawner.Pause();
        uiHandler.PauseUI();
        EnableSwords(false);
        EnableUiPointers(true);
        isPaused = true;
        foreach (AnimationHandler animated in animateds)
        {
            animated.Idle();
        }
        Debug.Log("Game Paused.");
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        mainAudioSource.UnPause();
        spawner.UnPause();
        uiHandler.HideAll();
        EnableSwords(true);
        EnableUiPointers(false);
        isPaused = false;
        foreach (AnimationHandler animated in animateds)
        {
            animated.Dance();
        }
        Debug.Log("Game Resumed.");
    }

    // Back to main menu
    public void EndGame(bool isWin)
    {
        clipLength = 0;
        uiHandler.ChooseSongUI();
        EnableSwords(false);
        EnableUiPointers(true);
        mainAudioSource.Stop(); 
        spawner.Stop();
        themeAudioSource.Play();
        
        foreach (AnimationHandler animated in animateds)
        {
            animated.Idle();
        }



        if (isWin)
        {
            scoreHandler.SaveScore();
        }
        else
        {
            spawner.Stop();
        }
    }

    private void StartSongWithDelay()
    {
        mainAudioSource.Play();
        
        foreach (AnimationHandler animated in animateds)
        {
            animated.Dance();
        }
    }

    public void EnableSwords(bool enable)
    {
        for (int i = 0; i < swords.Length; ++i)
        {
            swords[i].SetActive(enable);
        }
    }

    public void EnableUiPointers(bool enable)
    {
        for (int i = 0; i < uiPointers.Length; ++i)
        {
            uiPointers[i].SetActive(enable);
        }
    }
}

