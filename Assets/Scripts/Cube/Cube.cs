﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public float speed = -10;

    public float skippingZPosition = -2f;

    public bool isRed;

    private Vector3 hittingDirectionVector;
    public float similaritySensitivity = .05f;

    private float lifetime = 0.0f;

    [HideInInspector]
    public ScoreHandler scoreHandler;

    public GameObject redExplosionPrefab;
    public GameObject blueExplosioPrefab;
    public AudioClip explosionSound;
    public GameObject subcubePrefab;

    private void Start()
    {
        // Reference points of the cube used to determine its hitting direction
        Transform entryHitPoint = transform.GetChild(0);
        Transform exitHitPoint = transform.GetChild(1);
       
        hittingDirectionVector = (exitHitPoint.position - entryHitPoint.position).normalized;
    }

    void Update()
    {
        lifetime += Time.deltaTime;

        // Keep on movin
        transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));

        // Check if too far behind
        if (transform.position.z < skippingZPosition)
            Skipped();
    }
      
    // Collider is placed just above the box, cut is done on exit to ensure only the right face is hit
    private void OnTriggerExit(Collider other)
    {
        Sword sword = other.gameObject.GetComponent<Sword>();

        if (sword != null && sword.isRed == this.isRed)
        {
            // Cube is destroy if the sword movement vector and the cube's hitting vector are somewhat similar
            if (Vector3.Dot(sword.GetMovementVector(), hittingDirectionVector) > similaritySensitivity)
            {
                //SetUp(!isRed, null); //Debug purposes only
                if (scoreHandler)
                    scoreHandler.AddPoints();

                sword.StartHapticFeedback(true);
                Explode();
            }
        }
    }

    private void Skipped()
    {
        scoreHandler.SubtractPoints();
        Destroy(gameObject);
    }

    public void SetUp(bool aIsRed, ScoreHandler aScoreHandler)
    {
        //Set Color
        isRed = aIsRed;
        Material mat = GetComponent<MeshRenderer>().material;
        Color color = aIsRed ? Color.red : Color.blue;
        mat.color = color;
        mat.SetColor("_EmissionColor", color);

        // Set Color in subcubes
        ExplodingSubcube[] ess = GetComponentsInChildren<ExplodingSubcube>(true);
        foreach (ExplodingSubcube es in ess)
        {
            es.SetColor(isRed);
        }

        //Set Game related
        scoreHandler = aScoreHandler;
    }

    public void Delete()
    {
        Destroy(gameObject);
    }

    public void Explode()
    {
        //GetComponent<AudioSource>().Play();

        GameObject explosionPrefab = isRed ? redExplosionPrefab : blueExplosioPrefab;
        Instantiate(explosionPrefab, transform).transform.position = transform.position;
        Instantiate(subcubePrefab, transform).transform.position = transform.position;

        speed = 0;
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<MeshRenderer>().enabled = false;
        SetUp(!isRed, null); //Debug purposes only
        Invoke("Delete", 1f);
    }
}
