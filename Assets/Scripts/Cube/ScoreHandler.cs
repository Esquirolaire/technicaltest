﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreHandler : MonoBehaviour
{
    // TODO make this accessible by editor but not by code
    public int baseScore = 10;
    public int negativeScore = 100;
    
    // TODO combo thing
    private int comboCounter = 0;

    public TextMeshProUGUI title;
    public TextMeshProUGUI highestScoreText;
    public TextMeshProUGUI currentScoreText;

    private int highestScore;
    private int currentScore;

    // TODO delete this by registering and event at lost game.
    private LevelController levelController;

    // Prefix to avoid problems with potential song names coincidences
    private string playerPrefsPrefix = "SCORE";

    private void Awake()
    {
        levelController = FindObjectOfType<LevelController>();
    }

    // Start is called before the first frame update
    public void Start()
    {       
        currentScore = 0;
        currentScoreText.text = currentScore.ToString();
    }

    public void ClearProgress()
    {
        title.text = "";
        highestScoreText.text = "0";
        currentScoreText.text = "0";
    }

    public void AddPoints()
    {
        currentScore += baseScore;
        UpdateScore();
    }

    public void SubtractPoints()
    {
        currentScore -= negativeScore;
        UpdateScore();
    }

    private void UpdateScore()
    {
        if (currentScore < 0)
        {
            currentScore = 0;
            levelController.EndGame(false);
        }

        currentScoreText.text = currentScore.ToString();
    }

    public void SaveScore()
    {
        if (currentScore > highestScore)
        {
            highestScore = currentScore;
            highestScoreText.text = currentScore.ToString();
            string key = playerPrefsPrefix + title.text;
            PlayerPrefs.SetString(key, highestScore.ToString());
            //TODO some confetti and nice sounds;
        }
    }

    public void SetupScoreboard(string song)
    {
        // Load Song title on UI
        title.text = song;

        // Set up current score
        currentScore = 0;
        currentScoreText.text = currentScore.ToString();

        // Load Song Highscore when available
        string key = playerPrefsPrefix + title.text;
        string value = PlayerPrefs.GetString(key);
        
        if (value.Length > 0)
        {
            highestScore = int.Parse(value);
            highestScoreText.text = value;
        }
        else
        {
            highestScore = 0;
            highestScoreText.text = "0";
        }       
    } 
}
