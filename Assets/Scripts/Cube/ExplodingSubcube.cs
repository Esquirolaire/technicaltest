﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingSubcube : MonoBehaviour
{
    Rigidbody r;
    public float force = 100f;
    public float sideFactor = 2f;
    public float randomFactor = 0.25f;
    // Start is called before the first frame update
    void Start()
    {
        r = GetComponent<Rigidbody>();
    }

    public void SetColor(bool isRed)
    {
        GetComponent<MeshRenderer>().material.color = isRed ? Color.red : Color.blue;
    }
    private void OnTriggerEnter(Collider other)
    {
        //Since subcubes are simmetrical, they take local position to decide to go left or right
        float x = Random.Range(0, randomFactor);
        float y = Random.Range(0, randomFactor);
        float z = Random.Range(0, randomFactor);

        Vector3 forceDirection = transform.localPosition * sideFactor + Vector3.up + new Vector3(x,y,z);
        forceDirection.Normalize();
        Debug.Log(forceDirection);
        r.AddForce(forceDirection * force, ForceMode.Impulse);
    }
}
