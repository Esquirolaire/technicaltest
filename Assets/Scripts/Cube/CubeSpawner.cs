﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class CubeSpawner : MonoBehaviour
{
    public float distanceFromCenter = 0.4f;
    public float circleRadius = 0.3f;
    public float circleDivisions = 8;
    
    private float bonusDoubleCubeSeparation = 0.15f;

    private int bpm;
    private float beatInterval;
    private float timeAcum;
    public float maxBeatDivision = 2f;

    public Cube cubePrefab;

    private bool kick = false;
    private bool previousKick = false;
    private bool snare = false;
    private bool previousSnare = false;
    private bool hihat = false;
    private bool previousHihat = false;
    private bool energy = false;
    private bool previousEnergy = false;

    private bool previousWasRed = false;
    private float chanceFactor = 0.125f;

    private ScoreHandler scoreHandler;
    private AudioSource audioSource; // Not for hearing, beat analyzing only
    private LightManager lightManager;

    public void BeatCallbackEventHandler(BeatDetection.EventInfo eventInfo)
    {
        switch (eventInfo.messageInfo)
        {
            case BeatDetection.EventType.Energy:
                energy = true;
                break;
            case BeatDetection.EventType.HitHat:
                hihat = true;
                break;
            case BeatDetection.EventType.Kick:
                kick = true;
                break;
            case BeatDetection.EventType.Snare:
                snare = true;
                break;
        }
    }

    private void Update()
    {
        timeAcum += Time.deltaTime;

        // Cubes are only spawned on beat multiples
        if (audioSource.isPlaying && timeAcum > beatInterval)
        {
            if (energy || kick || snare || hihat)
            {
                SpawnCubes();
                timeAcum = 0.0f;
                lightManager.ToggleLights();
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        scoreHandler = FindObjectOfType<ScoreHandler>();
        lightManager = FindObjectOfType<LightManager>();
        audioSource = GetComponent<AudioSource>();
    }

    public void SetupSpawner(AudioClip aClip)
    {
        audioSource.clip = aClip;

        // Determine the beat
        GetComponent<BeatDetection>().CallBackFunction = BeatCallbackEventHandler;
        bpm = UniBpmAnalyzer.AnalyzeBpm(GetComponent<AudioSource>().clip);
        Debug.Log("BPM is: " + bpm);
        beatInterval = 60f / (float)bpm;
        beatInterval /= maxBeatDivision;
        //Debug.Log("beatInterval is: " + beatInterval);
    }

    public void Init()
    {
        audioSource.Play();
    }

    public void Pause()
    {
        audioSource.Pause();
    }

    public void UnPause()
    {
        audioSource.UnPause();
    }

    public bool IsWorking()
    {
        return audioSource.isPlaying;
    }

    public void Stop()
    {
        audioSource.Stop();

        Cube[] cubes = transform.GetComponentsInChildren<Cube>();
        
        for (int i = 0; i < cubes.Length; ++i)
        {
            // TODO delay en cascada
            cubes[i].Delete();
        }
    }

    private void SpawnCubes()
    {
        // Randomness determination        
        bool isRed = DecideColor();
        previousWasRed = isRed;

        // Basic cube placement    
        Cube cube = PlaceCube(isRed);

        // Bonus cube placement
        if (energy && previousEnergy && kick && previousKick && 
            snare && previousSnare && hihat && previousHihat)
        {
            // TODO Quadruple cubes
        }
        else if (energy && kick && snare && hihat)
        {
            BonusDoubleCubes(cube, isRed);
        }
        else if ((energy || kick)  && (snare || hihat))
        {
            BonusTwinOppositeCube(cube, isRed);
        }
        else
        {
            RotateCube(cube);
        }

        previousEnergy = energy;
        energy = false;
        previousKick = kick;
        kick = false;
        previousSnare = snare;
        snare = false;
        previousHihat = hihat;
        hihat = false;
    }

    private void BonusTwinOppositeCube (Cube originalcube, bool isRed)
    {
        Cube newCube = PlaceCube(!isRed);

        List<Cube> cubes = new List<Cube>();
        cubes.Add(originalcube);
        cubes.Add(newCube);

        RotateCube(cubes);
    }

    private void BonusDoubleCubes(Cube originalCube, bool isRed)
    {
        Vector3 centralPoint = originalCube.transform.position;
        originalCube.transform.Translate(new Vector3(0f, -circleRadius, 0f));

        //Place the new cube
        Cube newCube = Instantiate(cubePrefab);
        newCube.SetUp(!isRed, scoreHandler);
        newCube.transform.parent = this.transform;
        newCube.transform.position = originalCube.transform.position; // Place like the other ocube

        // Place cubes next to each other
        float separation = isRed ? -bonusDoubleCubeSeparation : bonusDoubleCubeSeparation;
        originalCube.transform.Translate(new Vector3(separation, 0f, 0f));
        newCube.transform.Translate(new Vector3(-separation, 0f, 0f));

        // Rotate the cubes
        int rot = 360 / (int)circleDivisions * Mathf.FloorToInt(Random.Range(0, circleDivisions));
        originalCube.transform.RotateAround(centralPoint, Vector3.forward, rot);
        newCube.transform.RotateAround(centralPoint, Vector3.forward, rot);

        // Swap cubes when to avoid the need of crossing hands
        if (rot >= 90 && rot < 270)
        {
            Vector3 temp = originalCube.transform.position;
            originalCube.transform.position = newCube.transform.position;
            newCube.transform.position = temp;
        }
    }

    private int RotateCube(Cube cube)
    {
        // Spawning in discrete steps of a circle
        Vector3 centralPoint = cube.transform.position;
        cube.transform.Translate(new Vector3(0f, -circleRadius, 0f));
        int rotation = 360 / (int)circleDivisions * Mathf.FloorToInt(Random.Range(0, circleDivisions));
        cube.transform.RotateAround(centralPoint, Vector3.forward, rotation);

        // return rotation for recycling purposes
        return rotation;
    }

    // Rotate Several cubes with the same Rotation
    private int RotateCube(List<Cube> cubes)
    {
        int rotation = RotateCube(cubes[0]);

        for (int i = 1; i < cubes.Count; ++i)
        {
            Vector3 centralPoint = cubes[i].transform.position;
            cubes[i].transform.Translate(new Vector3(0f, -circleRadius, 0f));
            cubes[i].transform.RotateAround(centralPoint, Vector3.forward, rotation);
        }

        return rotation;       
    }

    private Cube PlaceCube(bool isRed)
    {
        // Basic placement    
        Cube cube = Instantiate(cubePrefab);
        cube.SetUp(isRed, scoreHandler);
        cube.transform.parent = this.transform;
        cube.transform.position = this.transform.position;
        cube.transform.Translate(new Vector3(isRed ? -distanceFromCenter : distanceFromCenter, 0f, 0f));

        return cube;
    }

    private bool DecideColor()
    {
        // High chance means blue
        float chance = 0.5f;

        // Lows tend to be red
        if (kick) chance -= chanceFactor;
        if (previousKick) chance -= chanceFactor * 2f;

        // Higher notes tend to be blue
        if (snare || hihat) chance += chanceFactor;
        if (previousSnare || previousHihat) chance += chanceFactor * 2f;

        // It tends to change color
        if (previousWasRed) chance += chanceFactor * 2;
        else chance -= chanceFactor * 2;

        return Random.Range(0f, 1f) > chance ? true : false;
    }
}
