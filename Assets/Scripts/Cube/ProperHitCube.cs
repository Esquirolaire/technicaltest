﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProperHitCube : MonoBehaviour
{
    [HideInInspector]
    public bool isHit = false;

    protected void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Sword>().isRed == GetComponentInParent<Cube>().isRed)
        {
            isHit = true;
        }
    }
}
