﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour
{
    public bool isRed = false;

    private Transform tip;
    private Vector3 lastTipPosition;
    private AudioSource audioSource;

    // A trail of the points is kept so it is possible to know which direction the sword is moving
    public int historicValuesAmount = 3;
    private List<Vector3> directionVectorHistoric = new List<Vector3>();
    private int historicIndex = 0;

    // Smaller movements than the threshold are ignored.
    public float movementThreshold = 0.1f; 
    [HideInInspector]
    public Vector3 directionVector;

    public GameObject blueSparkPrefab;
    public GameObject redSparkPrefab;
    private GameObject sparks;
    public float particleTime = 0.1f;
    float particleTimeAcum = 0f;
    private void Start()
    {
        // Color set up
        Material mat = GetComponent<MeshRenderer>().material;
        Color color = isRed ? Color.red : Color.blue;
        mat.color = color;
        mat.SetColor("_EmissionColor", color);

        // trail initialization
        tip = transform.GetChild(0);
        lastTipPosition = tip.position;

        audioSource = GetComponent<AudioSource>();
        sparks = isRed ? redSparkPrefab : blueSparkPrefab;

        for (int i = 0; i < historicValuesAmount; ++i)
        {
            directionVectorHistoric.Add(Vector3.zero);
        }
    }

    private void Update()
    {
        CalculateMovementVector();
    }

    private void CalculateMovementVector()
    {
        directionVectorHistoric[historicIndex] = (tip.position - lastTipPosition).magnitude > movementThreshold ? (tip.position - lastTipPosition) : Vector3.zero;
        historicIndex = historicIndex < historicValuesAmount - 1 ? historicIndex + 1 : 0; 
        lastTipPosition = tip.position;
    }

    // Movement vector calculated on demand
    public Vector3 GetMovementVector()
    {
        Vector3 acum = new Vector3(0, 0, 0);

        for (int i = 0; i < historicValuesAmount; ++i)
        {
            acum += directionVectorHistoric[i];
        }

        return acum.normalized;
    }

    // Manage Collision Feedback
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Cube>() || other.gameObject.GetComponent<Sword>())
        {
            Vector3 collisionPoint = other.ClosestPoint(transform.position);
            GameObject go = Instantiate(blueSparkPrefab);
            go.transform.position = collisionPoint;
            
            if (other.gameObject.GetComponent<Sword>())
            {
                StartHapticFeedback(false);
                audioSource.Play();
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<Sword>())
        {
            particleTimeAcum += Time.deltaTime;
            if (particleTimeAcum > particleTime)
            {
                Vector3 collisionPoint = other.ClosestPoint(transform.position);
                GameObject go = Instantiate(sparks);
                go.transform.position = collisionPoint;
                particleTimeAcum = 0f;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Sword>())
        {
            audioSource.Stop();
            StopHapticFeedback();
        }
    }
    public void StartHapticFeedback(bool isEnding)
    {
        // red is left
        if (isRed)
        {
            OVRInput.SetControllerVibration(.2f, .8f, OVRInput.Controller.LTouch);
        }
        else
        {
            OVRInput.SetControllerVibration(.2f, .8f, OVRInput.Controller.RTouch);
        }

        if (isEnding)
        {
            Invoke("StopHapticFeedback", 0.3f);
        }
    }

    public void StopHapticFeedback()
    {
        // red is left
        if (isRed)
        {
            OVRInput.SetControllerVibration(0, 0f, OVRInput.Controller.LTouch);
        }
        else
        {
            OVRInput.SetControllerVibration(0, 0f, OVRInput.Controller.RTouch);
        }
    }

    public void PlaySoundOnce(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }

}
