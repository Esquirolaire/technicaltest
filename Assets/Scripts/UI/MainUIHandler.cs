﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainUIHandler : MonoBehaviour
{
    public GameObject startPanel;
    public GameObject pausePanel;
    public GameObject configPanel;
    public GameObject startButton;

    private AudioSource uiAudioSource;

    private void Start()
    {
        uiAudioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    public void ChooseSongUI()
    {
        startPanel.SetActive(true);
        pausePanel.SetActive(false);
        configPanel.SetActive(false);
    }

    public void PauseUI()
    {
        startPanel.SetActive(false);
        pausePanel.SetActive(true);
        configPanel.SetActive(false);
    }

    public void ConfigUI()
    {
        startPanel.SetActive(false);
        pausePanel.SetActive(false);
        configPanel.SetActive(true);
    }

    public void HideAll()
    {
        startPanel.SetActive(false);
        pausePanel.SetActive(false);
        configPanel.SetActive(false);
    }

    public void EnableStartButton()
    {
        startButton.SetActive(true);
    }

    public void MakeClickSound()
    {
        uiAudioSource.Play();
    }
}
