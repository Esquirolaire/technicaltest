﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateImage : MonoBehaviour
{

    public float speed = 2f;

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(Vector3.forward, Time.deltaTime * -speed);
    }
}
