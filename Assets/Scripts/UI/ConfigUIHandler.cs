﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class ConfigUIHandler : MonoBehaviour
{
    public AudioMixer mixer;
    public Slider musicVolumeSlider;
    private string musicVolumeString = "MusicVolume";

    public Slider musicFXSlider;
    private string musicFXString = "FXVolume";

    private bool isComingFromMainMenu = false;
    private MainUIHandler mainUIHandler;

    public void Start()
    {
        musicVolumeSlider.value = PlayerPrefs.GetFloat(musicVolumeString);
        musicFXSlider.value = PlayerPrefs.GetFloat(musicFXString);
        mainUIHandler = FindObjectOfType<MainUIHandler>();
    }

    public void UpdateMusicVolume()
    {
        mixer.SetFloat(musicVolumeString, musicVolumeSlider.value);
        PlayerPrefs.SetFloat(musicVolumeString, musicVolumeSlider.value);
    }

    public void UpdateFXVolume()
    {
        mixer.SetFloat(musicFXString, musicFXSlider.value);
        PlayerPrefs.SetFloat(musicFXString, musicFXSlider.value);
    }

    // Trace origin so we can go back
    public void ComingFromMainMenu(bool b)
    {
        isComingFromMainMenu = b;
    }

    // Clear progress and update all UI elements
    public void ClearProgress()
    {
        PlayerPrefs.DeleteAll();
        FindObjectOfType<ScoreHandler>().ClearProgress();
        Start();
    }

    // Go to previous panel
    public void GoBack()
    {
        if (isComingFromMainMenu)
        {
            mainUIHandler.ChooseSongUI();
        }
        else
        {
            mainUIHandler.PauseUI();
        } 
    }
}
