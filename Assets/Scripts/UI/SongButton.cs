﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SongButton : MonoBehaviour
{
    [HideInInspector]
    public AudioClip clip;

    LevelController levelController;
    MainUIHandler mainUIHandler;

    public void Start()
    {
        levelController = FindObjectOfType<LevelController>();
        mainUIHandler = FindObjectOfType<MainUIHandler>();
    }

    public void SetUpButton(AudioClip aClip, string aSong, string aArtist)
    {
        clip = aClip;

        TextMeshProUGUI[] textHolders = transform.GetComponentsInChildren<TextMeshProUGUI>();
        textHolders[0].text = aSong;
        textHolders[2].text = aArtist;

        GetComponent<Toggle>().onValueChanged.AddListener(delegate { SetupGame(aSong, aClip); }); 
    }

    public void SetupGame(string aSong, AudioClip aClip)
    {
        mainUIHandler.MakeClickSound();
        levelController.SetupGame(aSong, aClip);           
    }
}
