﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Dinamically loads mp3 files from Resources folder
public class SongLoader : MonoBehaviour
{
    private AudioImporter importer;

    private Dictionary<string, AudioClip> audioClips;

    public GameObject songButtonPrefab;
    public GameObject songScrollableList;
    public GameObject loadedUI;
    public GameObject loadingUI;

    private void Start()
    {
        importer = GetComponent<NAudioImporter>();
        audioClips = new Dictionary<string, AudioClip>();

        // Looking for audio files in Resources folder
        string path = Application.dataPath + "/Resources";
        string[] filespath = System.IO.Directory.GetFiles(path);

        List<string> songpaths = new List<string>();
        
        //Just loading mp3s        
        foreach (string filepath in filespath)
        {
            if (filepath.Contains(".mp3") && !filepath.Contains(".meta"))
            {
                songpaths.Add(filepath);
            }
        }

        Debug.Log("Loading Audio Clips...");
        StartCoroutine(ImportClips(songpaths));
    }

    IEnumerator ImportClips(List<string> songpaths)
    {
        foreach (string songpath in songpaths)
        {
            yield return StartCoroutine(Import(songpath));
        }

        LoadSongsInUI();
    }

    IEnumerator Import(string songpath)
    {
        float startTime = Time.time;
        Debug.Log("Loading :" + songpath);

        importer.Import(songpath);

        while (!importer.isDone && !importer.isError)
            yield return null;
        

        if (importer.isError)
        {
            Debug.LogError(importer.error);
        }
        else
        {
            Debug.Log("Finished loading " + songpath);
            Debug.Log("It took " + (Time.time-startTime));
            string song = songpath.Split('\\')[songpath.Split('\\').Length - 1].Replace(".mp3", "");
            audioClips.Add(song, importer.audioClip);
        }

        // give some time to properly import the songs
        //yield return new WaitForSeconds(2f);
    }

    private void LoadSongsInUI()
    {
        Debug.Log("Loading song in UI");

        // Create one Button fore each song
        foreach(KeyValuePair<string, AudioClip> clip in audioClips)
        {
            string[] filename = clip.Key.Split('-');

            GameObject songButton = Instantiate(songButtonPrefab, songScrollableList.transform);
            
            // In case expected format is not matched
            if (filename.Length > 1)
            {
                songButton.GetComponent<SongButton>().SetUpButton(clip.Value, filename[1].Replace("_", " "), filename[0].Replace("_", " "));
            }
            else
            {
                songButton.GetComponent<SongButton>().SetUpButton(clip.Value, filename[0].Replace("_", " "), "");
            }
        }

        loadingUI.SetActive(false);
        loadedUI.SetActive(true);
    }
}
