﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Autodestroy : MonoBehaviour
{
    public float lifetime = 1f; 

    // Start is called before the first frame update
    void Start()
    {
        Invoke("Die", lifetime);
    }

    private void Die()
    {
        Destroy(gameObject);
    }
}
