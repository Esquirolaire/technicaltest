﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightManager : MonoBehaviour
{
    private List<Light> lights;
    private bool isRed = false;

    // Start is called before the first frame update
    void Start()
    {
        Light[] tmp = FindObjectsOfType<Light>();
        lights = new List<Light>();

        foreach (Light light in tmp)
        {
            if (light.type.Equals(LightType.Spot))
            {
                lights.Add(light);
            }
        }
    }

    // Toggles colors of the light
    public void ToggleLights()
    {
        foreach (Light light in lights)
        {
            light.color = isRed ? Color.red : Color.blue;
        }

        isRed = !isRed;
    }
}
